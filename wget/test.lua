local dfpwm = require("cc.audio.dfpwm")
local speaker = peripheral.find("speaker")

local files = {
    "disk/part1.dfpwm",
    "disk2/part2.dfpwm"
}

local decoder = dfpwm.make_decoder()
for _, file in ipairs(files) do
	for chunk in io.lines(file, 16 * 1024) do
		local buffer = decoder(chunk)
		while not speaker.playAudio(buffer) do
			os.pullEvent("speaker_audio_empty")
		end
	end
end