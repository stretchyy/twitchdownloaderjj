-- downloads a file, given a raw url link. only accepts *.wav right now
-- pilfered from the 
local function download.raw(...)
	local path = ...
	local data
	if path:match("^https?://") then
		local handle, err = http.get(path, nil, true)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		local code = handle.getResponseCode()
		if code ~= 200 then handle.close() error("Could not connect to " .. path .. ": HTTP " .. code) end
		data = handle.readAll()
		handle.close()
	elseif path:match("^wss?://") then
		local handle, err = http.websocket(path)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		data = ""
		repeat
			local ev, url, msg, bin = os.pullEvent()
			if ev == "websocket_message" and url == path then
				data = data .. msg
				if not bin then print("Warning: A text message was sent. This data may have been corrupted.") end
			end
		until ev == "websocket_closed" and url == path
	else
		path = shell.resolve(...)
		local file, err = fs.open(path, "rb")
		if not file then error("Could not open " .. path .. ": " .. err) end
		data = file.readAll()
		file.close()
	end
	return data
end