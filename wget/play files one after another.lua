local files = {
  "disk1/part1.dfpwm",
  "disk2/part2.dfpwm",
  "disk3/part3.dfpwm"
}
for _, file in ipairs(files) do
  shell.run("speaker play", file)
end