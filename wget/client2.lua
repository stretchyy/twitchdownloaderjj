-- client V2
-- the only thing the client will do is run aukit.play() to reduce timy syncronization disparity


print("running client v2.0.2")
--keep using the OG aukit until cant anymore.
if not fs.exists("aukit.lua") then 
	shell.run("wget https://github.com/MCJack123/AUKit/raw/master/aukit.lua")
end
local aukit = require "aukit"
local modem = peripheral.find("modem") or error("No modem attached", 0)
local speaker = peripheral.find("speaker") or error("No speaker attached", 0)
shell.run("bg client_response.lua")
modem.open(15)

--might still need round function for time parity if out of sync too much
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

print("listening on channel 15....")
local song
while true do
	local event, side, channel, replyChannel, message, distance
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	until channel == 15
	print("message received")
	song = message
	for _,v in ipairs(message) do
		print("playing pt".._)
		aukit.play(v,speaker)
	end
	print("debug-1")
end
