-- client discovery response
local modem = peripheral.find("modem") or error("No modem attached", 0)
modem.open(16)

local clientFiles = {
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/HEAD/wget/client2.lua",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/HEAD/wget/client_response.lua"
}
print("v2.0.2")
print("listening")
while true do
	local event, side, channel, replyChannel, message, distance
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	until channel == 16
	if message == true then 
		print("ping from channel "..replyChannel)
		modem.transmit(replyChannel,16,os.epoch("local"))
	elseif message == false then os.reboot()
	elseif message == "update" then
		print("Updating client...")
		for _,v in ipairs(clientFiles) do
			local data
			local handle, err = http.get(v,nil,true)
			if not handle then error("Could not connect to " .. path .. ": " .. err) end
			local code = handle.getResponseCode()
			if code ~= 200 then handle.close() error("Could not connect to " .. path .. ": HTTP " .. code) end
			data = handle.readAll()
			handle.close()
			local filename = v:match("client.+%.lua$")
			fs.delete(filename)
			local file = fs.open(filename,"w")
			file.write(data)
			file.close()
			print("updated: "..filename)
		end
		os.reboot()
	elseif message[1] == "lua" then 
		assert(loadstring(message[2]))
	else print("Unknown message. No handler.")
	end
end