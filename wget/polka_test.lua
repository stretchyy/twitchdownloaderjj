-- **heavily** based on the aukit program. just cannibalised it a bit. modified things that needed modifying


if not fs.exists("aukit.lua") then 
	shell.run("wget https://github.com/MCJack123/AUKit/raw/master/aukit.lua")
end
local expect = require "cc.expect"
local aukit = require "aukit"
--local modem = peripheral.find("modem") or error("No modem attached", 0)
local speaker = peripheral.find("speaker") or error("No speaker attached", 0)
local sample = "https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song6/part%02d.wav"
local parcel = {}

-- downloads a file, given a raw url link. only accepts *.wav right now
-- pilfered from the 
local function downloadSegment(...)
	local path = ...
	local data
	if path:match("^https?://") then
		local handle, err = http.get(path, nil, true)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		local code = handle.getResponseCode()
		if code ~= 200 then handle.close() error("Could not connect to " .. path .. ": HTTP " .. code) end
		data = handle.readAll()
		handle.close()
	elseif path:match("^wss?://") then
		local handle, err = http.websocket(path)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		data = ""
		repeat
			local ev, url, msg, bin = os.pullEvent()
			if ev == "websocket_message" and url == path then
				data = data .. msg
				if not bin then print("Warning: A text message was sent. This data may have been corrupted.") end
			end
		until ev == "websocket_closed" and url == path
	else
		path = shell.resolve(...)
		local file, err = fs.open(path, "rb")
		if not file then error("Could not open " .. path .. ": " .. err) end
		data = file.readAll()
		file.close()
	end
	return data
end

--downlaods the whole song in 5 second, 488kb segments, into a list. makes a 40-50Mb "variable/table"
local song = {}
local function downloadSong()
	local x = 0
	repeat
		local arg = string.format(sample,x)
		local status, retval = pcall(downloadSegment,arg)	
		if status then
			table.insert(song,retval)
			print("segment pt"..x)
		else print("segments: "..x-1) end
		x=x+1 
	until not status
	if x==0 then
		print("something went wrong, try without 'pcall'")
	end
	return song
end

-- package up the audio chunk into a list instead of playing it
function cannibal(callback) 
    expect(1, callback, "function") --[[i dont... particularly know what this does... 
	i mean, i know what it DOES....
	just not why it needs to exist *here*... or exist anymore...
	but i imported expect at the top anyway]]--
    local chunks = {}
    local complete = false
    parallel.waitForAll(function()
        for chunk in callback do chunks[#chunks+1] = chunk sleep(0) end
        complete = true
    end, -- ends first parallel function
	function()
        while not complete or #chunks > 0 do
            while not chunks[1] do sleep(0) end
            local chunk = table.remove(chunks, 1)
            -- instead of playing the chunk, insert the chunk into a list
			table.insert(parcel,chunk[1])
        end -- ends while not complete
    end) -- ends second parallel function
end -- ends cannibal.package
-- ill be honest, the original aukit.play function confuses the living shit out of me. 
-- after disecting it, breaking it down, testing each lines job....
-- ..... still no fucking clue...
-- i understand what each line "does", but not how each line interacts 
-- they say the most efficient code is the shortest, but is often the most complex...
-- damn fucking right....
-- my hack job is my best understanding, and it still probably wont work |-(


for _,song in ipairs(downloadSong()) do
	local s = os.epoch("local")
	local audio
	audio = aukit.wav(song)  -- for now only does wav until im sure the rest of the script works
	sleep(0)
	
	print("Resampling...")
	local resamp = audio:resample(48000)
	sleep(0)

	print("Converting to mono...")
	local mono = resamp:mono()
	sleep(0)

	print("Normalizing...")
	local normal = aukit.effects.normalize(mono, 0.8)
	sleep(0)
	-- resampling, converting to mono and normalizing was copypasta from auplay.lua
	
	print("Compiling pt.".._)
	cannibal(normal:stream(48000))
	--aukit.play(normal:stream(48000), speaker)
	print(os.epoch("local")-s)
end
-- by this point, we should have a table/list called parcel that contains song chunks that can be played directly through a speaker


-- taken from modified aukit.play, used to play chunks of audio.
--[[
local fn = {}
for i, v in ipairs(speakers) do fn[i] = function()
	local name = peripheral.getName(v)
		while not v.playAudio(chunk[i] or chunk[1]) do
			repeat until select(2, os.pullEvent("speaker_audio_empty")) == name
		end --ends while not v.playAudio
end end -- ends for i,v in pairs and fn[i] = function]]--

-- for future reference, this is what will be sent via rednet or transmit, to be played by the 'clients'
for j,k in ipairs(parcel) do
	local fn = {}
	for i,v in ipairs(speakers) do fn[i] = function()
		local name = peripheral.getName()
		while not speaker.playAudio(k) do
			repeat until select(2, os.pullEvent("speaker_audio_empty")) == name
		end
	end end
	parallel.waitForAll(table.unpack(fn))
end










