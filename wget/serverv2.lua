-- server V2
-- "mostly" rewrite of server functions
-- the cannibalisation of the Aukit program.
-- broadcast to network
if not fs.exists("aukit.lua") then 
	shell.run("wget https://github.com/MCJack123/AUKit/raw/master/aukit.lua")
end
print("Server host v2.0.2 rewrite")
local speaker = peripheral.find("speaker") or error("No speaker attached", 0)
local modem = peripheral.find("modem") or error("No modem attached", 0)
local aukit = require "aukit"
modem.open(43)



local speakerNetwork
local function speakerDiscovery()
	modem.transmit(16,43,true) -- broadcast 00:00:00:00:00:00
	halt = os.epoch("local") + 500 -- time after transmitting broadcast
	local event, side, channel, replyChannel, message, distance
	print("now:  "..os.epoch("local"))
	print("halt: "..halt)
	
	local function listen()
		speakerNetwork = 0 -- reset to 0 every time discovery() is run
		while true do -- keep listening for 2 seconds
			event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
			if channel == 43 then -- looking for any response, dont care about the message.
				speakerNetwork = speakerNetwork + 1 -- for every response, add one to the log
				print("reply from "..speakerNetwork)
			else print("I shouldn't be here yet. debug-2") end
			print(os.epoch("local"))
		end -- it shouldnt take more than 1 or 2 ingame ticks for all client computers to respond, 500ms should be more than enough
	end

	local function tick()
		while os.epoch("local") <= halt do
			os.sleep(0.1)
			print(halt-os.epoch("local"))
		end
	end
	parallel.waitForAny(listen, tick)
	
	if speakerNetwork == 0 then
		error("no speakers on the network!")
		print(speakerNetwork.." speaker clients on the network")
	end
	print("debug-1")
end


-- cannibalised from Austream.lua
local function downloadSegment(...)
	local path = ...
	local data
	local message = {}
	if path:match("^https?://") then
		local handle, err = http.get(path, nil, true)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		local code = handle.getResponseCode()
		if code ~= 200 then handle.close() error("Could not connect to " .. path .. ": HTTP " .. code) end
		data = handle.readAll()
		handle.close()
	elseif path:match("^wss?://") then
		local handle, err = http.websocket(path)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		data = ""
		repeat
			local ev, url, msg, bin = os.pullEvent()
			if ev == "websocket_message" and url == path then
				data = data .. msg
				if not bin then print("Warning: A text message was sent. This data may have been corrupted.") end
			end
		until ev == "websocket_closed" and url == path
	else
		path = shell.resolve(...)
		local file, err = fs.open(path, "rb")
		if not file then error("Could not open " .. path .. ": " .. err) end
		data = file.readAll()
		file.close()
	end
	return data
end

local function mainUi()
	local urls = {}
	-- local track = ...

	local tracks = {
		"Cosmic Latte - The Next Eastwood, 4:10",
		"8-bit Band - Lonely Rolling Star, 5:43",
		"Christopher Tin - Baba Yetu, 3:30",
		"Pendulmum - Salt in the Wounds, 6:39",
		"Row Row Fight The Power!, 4:49",
		"Sea Shanty 2, 1:50",
		"Wagner - Bridal Chorus, 2:38",
		"Wild Child - Crazy Bird, 3:50",
		"Don't Stop Movin' the Hot Stuff - Donna Summer Club 7, 3:49",
		"Badly Playde Bridal Chorus, 2:01",
		"Disturbed - Down with the Sickness, 4:38",
		"Mariah Manson - All I Want for Christmas is the Beautiful People, 3:13"
		
	}
	local segments = {
		13,
		68,
		42,
		79,
		57,
		22,
		31,
		46,
		45,
		24,
		55,
		38
	}

	local start = {
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song1/Cosmic%20Latte%20-%20The%20Next%20Eastwood-%02d.dfpwm",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song2/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song3/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song4/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song5/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song6/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song7/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song8/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song9/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song10/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song11/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song12/part%o2d.wav"
	}
	
	speakerDiscovery() -- startup discovery
	
	while true do
		--term.clear()
		--term.setCursorPos(1,1)
		
		print("Select track number: ")
		for _,v in ipairs(tracks) do
			print(_..":		"..v)
		end
		local input = tonumber(read())
		if input > #tracks then
			print("track "..input.." doesnt exist")
			break
		end
		print ("now playing ")
		print (tracks[input])
		
		--term.clear()
		--term.setCursorPos(1,1)
		
		for i=0,segments[input] do
			table.insert(urls, string.format(start[input],i))
		end
		
		local song = {}
		for _,v in ipairs(urls) do
			local data = downloadSegment(v)
			local audio = aukit.wav(data)
			aukit.play(audio,speaker)
		end
		for _,k in ipairs(song) do
			aukit.play(k,speaker)
		end
		--modem.transmit(15,43,song) --broadcast song to computer listening on ch. 15
		
		-- modem.transmit(16,43,xyz) -- for xyz; true is ping for discovery, false restart clients, {lua,loadstring}
		-- modem.transmit(16, 43, true)
		
		-- clear url's table
		count = #urls
		for i=0,count do 
			urls[i]=nil 
		end
		speakerDiscovery() -- see how many speaker computers are listening on the network
	end
end

mainUi()