local dfpwm = require("cc.audio.dfpwm")
local speaker = peripheral.find("speaker")

local files = {
    "disk/part1.dfpwm",
    "disk2/part2.dfpwm",
    "disk3/part3.dfpwm",
    "disk4/part4.dfpwm",
    "disk5/part5.dfpwm",
    "disk6/part6.dfpwm",
    "disk7/part7.dfpwm",
    "disk8/part8.dfpwm",
    "disk9/part9.dfpwm",
    "disk10/part10.dfpwm",
    "disk11/part11.dfpwm",
    "disk12/part12.dfpwm",
    "disk13/part13.dfpwm"
}

local decoder = dfpwm.make_decoder()
for _, file in ipairs(files) do
	for chunk in io.lines(file, 16 * 1024) do
		local buffer = decoder(chunk)
		while not speaker.playAudio(buffer) do
			os.pullEvent("speaker_audio_empty")
		end
	end
end