-- play backup versions

-- functionally untouched aukit.play, just some comments to understand it
function aukit.play(callback, ...)
    expect(1, callback, "function")
    local speakers = {...}
    local chunks = {}
    local complete = false
    parallel.waitForAll(function()
        for chunk in callback do chunks[#chunks+1] = chunk sleep(0) end
        complete = true
    end, -- ends first parallel function
	function()
        while not complete or #chunks > 0 do
            while not chunks[1] do sleep(0) end
            local chunk = table.remove(chunks, 1)
            local fn = {}
            for i, v in ipairs(speakers) do fn[i] = function()
                local name = peripheral.getName(v)
                if config and not config.get("standardsMode") then
                    v.playAudio(chunk[i] or chunk[1], 3)
                    repeat until select(2, os.pullEvent("speaker_audio_empty")) == name
                else 
					while not v.playAudio(chunk[i] or chunk[1]) do
						repeat until select(2, os.pullEvent("speaker_audio_empty")) == name
					end --ends while not v.playAudio
				end -- ends if config
            end end -- ends for i,v in pairs and fn[i] = function
            parallel.waitForAll(table.unpack(fn))
        end -- ends while not complete
    end) -- ends second parallel function
end -- ends aukit.play

-- took out unnecessary code that functions "normally"..... "as intended"
function cannibal.play(callback, ...) 
    expect(1, callback, "function")
    local speakers = {...}
    local chunks = {}
    local complete = false
    parallel.waitForAll(function()
        for chunk in callback do chunks[#chunks+1] = chunk sleep(0) end
        complete = true
    end, -- ends first parallel function
	function()
        while not complete or #chunks > 0 do
            while not chunks[1] do sleep(0) end
            local chunk = table.remove(chunks, 1)
            
			local fn = {}
            for i, v in ipairs(speakers) do fn[i] = function()
                local name = peripheral.getName(v)
					while not v.playAudio(chunk[i] or chunk[1]) do
						repeat until select(2, os.pullEvent("speaker_audio_empty")) == name
					end --ends while not v.playAudio
            end end -- ends for i,v in pairs and fn[i] = function
            parallel.waitForAll(table.unpack(fn))
			
        end -- ends while not complete
    end) -- ends second parallel function
end -- ends aukit.play