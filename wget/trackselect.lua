local urls = {}
-- local track = ...

local tracks = {
	"Cosmic Latte - The Next Eastwood, 4:10",
	"8-bit Band - Lonely Rolling Star, 5:43",
	"Christopher Tin - Baba Yetu, 3:30",
	"Pendulmum - Salt in the Wounds, 6:39",
	"Row Row Fight The Power!, 4:49",
	"Sea Shanty 2, 1:50",
	"Wagner - Bridal Chorus, 2:38",
	"Wild Child - Crazy Bird, 3:50"
}
local segments = {
	13,
	68,
	42,
	79,
	57,
	22,
	31,
	46
}

start = {
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song1/Cosmic%20Latte%20-%20The%20Next%20Eastwood-%02d.dfpwm",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song2/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song3/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song4/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song5/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song6/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song7/part%02d.wav",
"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song8/part%02d.wav"
}

while true do
	term.clear()
    term.setCursorPos(1,1)
	
	print("Select track number: ")
	for _,v in ipairs(tracks) do
		print(_.." :"..v)
	end
	local input = tonumber(read())
	if input > #tracks then
		print("track "..input.." doesnt exist")
		break
	end
	print ("now playing ")
    print (tracks[input])
	
	for i=0,segments[input] do
		table.insert(urls, string.format(start[input],i))
	end
	
	for _, v in ipairs(urls) do
		print(v)
		--os.sleep(1)
		shell.run("austream", v)
	end
	count = #urls
	for i=0,count do 
		urls[i]=nil 
	end
end