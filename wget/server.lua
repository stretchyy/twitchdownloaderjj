-- server machine
print("V0.17.2")
local modem = peripheral.find("modem") or error("No modem attached", 0)
modem.open(43)

local speakerNetwork = 0 -- number of computers listening to channel #
local function discovery_alternate()
	modem.transmit(16,43,true) -- broadcast 00.00.00.00.00.00.00
	local halt = os.epoch("local") -- time after transmitting broadcast
	local event, side, channel, replyChannel, message, distance
	speakerNetwork = 0 -- reset to 0 every time discovery() is run
	repeat
		repeat
			event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
			if os.epoch("local") >= halt + 3000 then
				if speakerNetwork == 0 then
					error("Something broke. May as well quit now")
				end
				break
			end -- it shouldnt take more than 1 or 2 ingame ticks for all client computers to respond so quit early
		until channel == 43
		if os.epoch("local") >= halt + 500 then break end -- see similar break note above, break again cos dont want to add another +1 if channel reply wasnt recieved
		speakerNetwork = speakerNetwork + 1
		print(message)
	until os.epoch("local") >= halt + 500	
	print(speakerNetwork.." speaker clients on the network")
end
	
local function discovery()
	modem.transmit(16,44,true) -- broadcast 00:00:00:00:00:00
	local halt = os.epoch("local") -- time after transmitting broadcast
	local event, side, channel, replyChannel, message, distance
	speakerNetwork = 0 -- reset to 0 every time discovery() is run
	while os.epoch("local") >= halt + 500 do --
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
		if channel == 44 then -- looking for any response, dont care about the message.
			speakerNetwork = speakerNetwork + 1
			print("reply from "..speakerNetwork)
		end
	end -- it shouldnt take more than 1 or 2 ingame ticks for all client computers to respond, 500ms should be more than enough
	print(speakerNetwork.." speaker clients on the network")
end

local function stream_song(...)
	local path = ...
	local data
	local message = {}
	if path:match("^https?://") then
		local handle, err = http.get(path, nil, true)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		local code = handle.getResponseCode()
		if code ~= 200 then handle.close() error("Could not connect to " .. path .. ": HTTP " .. code) end
		data = handle.readAll()
		handle.close()
	elseif path:match("^wss?://") then
		local handle, err = http.websocket(path)
		if not handle then error("Could not connect to " .. path .. ": " .. err) end
		data = ""
		repeat
			local ev, url, msg, bin = os.pullEvent()
			if ev == "websocket_message" and url == path then
				data = data .. msg
				if not bin then print("Warning: A text message was sent. This data may have been corrupted.") end
			end
		until ev == "websocket_closed" and url == path
	else
		path = shell.resolve(...)
		local file, err = fs.open(path, "rb")
		if not file then error("Could not open " .. path .. ": " .. err) end
		data = file.readAll()
		file.close()
	end
	
	modem.transmit(15, 43, {path, data,os.epoch("local")}) -- broadcast the sound file
	local op = os.epoch("local")
	print(".wav segment broadcasted on channel 15") -- debug output to make sure we reach this point	
	-- wait for every computer on network to respond
	-- local butt-- maybe dont need this anymore?
	local i = 0
	print("Listening for responses, expecting "..speakerNetwork.." replies")
	repeat -- wait for every speaker computer on the network right now to send a response
		local event, side, channel, replyChannel, message, distance
		repeat
			event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
			if os.epoch("local") >= op + 15000 then -- if its been longer than 15 seconds
				modem.transmit(16,43,false) -- then chances are the client is stuck. this should reset the client computers
				print("It's been too long.")
				write("Press any key to continue")
				os.pullEvent("key")
			end 
		until channel == 43
		local ret = message[1]-op
		i = i + 1
		print("reply signal from computer: "..message[2])
		print("ping:"..ret.."ms")
	until i >= speakerNetwork
end

local function track_selection()
	local urls = {}
	-- local track = ...

	local tracks = {
		"Cosmic Latte - The Next Eastwood, 4:10",
		"8-bit Band - Lonely Rolling Star, 5:43",
		"Christopher Tin - Baba Yetu, 3:30",
		"Pendulmum - Salt in the Wounds, 6:39",
		"Row Row Fight The Power!, 4:49",
		"Sea Shanty 2, 1:50",
		"Wagner - Bridal Chorus, 2:38",
		"Wild Child - Crazy Bird, 3:50",
		"Don't Stop Movin' the Hot Stuff - Donna Summer Club 7, 3:49",
		"Badly Playde Bridal Chorus, 2:01",
		"Disturbed - Down with the Sickness, 4:38",
		"Mariah Manson - All I Want for Christmas is the Beautiful People, 3:13"
		
	}
	local segments = {
		13,
		68,
		42,
		79,
		57,
		22,
		31,
		46,
		45,
		24,
		55,
		38
	}

	local start = {
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song1/Cosmic%20Latte%20-%20The%20Next%20Eastwood-%02d.dfpwm",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song2/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song3/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/771b9b93a145d6d3c9909fab48ee2062c2b657a4/wget/song4/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song5/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song6/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song7/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/171ba586e60eceebf74cb384402838641f5e980e/wget/song8/part%02d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song9/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song10/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song11/part%o2d.wav",
	"https://bitbucket.org/stretchyy/twitchdownloaderjj/raw/30608b6606815c5df8f35e559803a8dbd2d86ed7/wget/song12/part%o2d.wav"
	}
	
	discovery() -- startup discovery
	
	while true do
		--term.clear()
		--term.setCursorPos(1,1)
		
		print("Select track number: ")
		for _,v in ipairs(tracks) do
			print(_..":		"..v)
		end
		local input = tonumber(read())
		if input > #tracks then
			print("track "..input.." doesnt exist")
			break
		end
		print ("now playing ")
		print (tracks[input])
		
		--term.clear()
		--term.setCursorPos(1,1)
		
		for i=0,segments[input] do
			table.insert(urls, string.format(start[input],i))
		end
		
		
		for _, v in ipairs(urls) do
			print(v)
			--os.sleep(1)
			--shell.run("austream", v)
			stream_song(v)
		end
		modem.transmit(16,43,false) -- restart clients
		count = #urls
		for i=0,count do 
			urls[i]=nil 
		end
		discovery() -- see how many speaker computers are listening on the network
	end
end

-- maybe gonna need this later if 
local function receive()
    local data = rednet.receive() -- here, the table is available with data.name and data.data from above
    local file = fs.open(data.name, "w")
    file.write(data.data)
    file.close()
end

local function send(file, id)
    local file = fs.open(file, "r")
    local data = file.readAll()
    file.close()
    rednet.send(id, {name = file, data = data}) -- note that we send a table with both the name and contents
end

local update_clients


local function host_listen()
	local event, side, channel, replyChannel, message, distance
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	until channel == 43
	loop = loop+1
	print("Received a reply: " .. tostring(message))
	dur = tonumber(message)
	ping = dur - sent
	ms = ping/72000
	print("time for computer "..loop.." to recieve: "..ms.." seconds")
end

track_selection()
print("unexpected exit.")
