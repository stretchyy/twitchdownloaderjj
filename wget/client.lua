-- client machine, music only.

-- first song start time isnt synced.
-- not all clients send back a response
print("V0.17.1")
if not fs.exists("aukit.lua") then 
	shell.run("wget https://github.com/MCJack123/AUKit/raw/master/aukit.lua")
end
local aukit = require "aukit"
local modem = peripheral.find("modem") or error("No modem attached", 0)
local lastPlayed
lastPlayed = 0 -- 5*math.ceil(os.clock()/5+0.5)
shell.run("bg client_response.lua")
modem.open(15)

-- gonna need this later for auto updating script
local function receive()
    local data = rednet.receive() -- here, the table is available with data.name and data.data from above
    local file = fs.open(data.name, "w")
    file.write(data.data)
    file.close()
end

print("channel open, waiting for signal")
while true do
	local event, side, channel, replyChannel, message, distance
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	until channel == 15
	print("New Segment... epoch: "..os.epoch("local"))
	path = message[1]
	data = message[2]
	now = message[3]
	-- later = message[4] -- was using this for time sync
	
	-- attempt to sync up clients to first song segment
	-- using os.clock is based on when computer was placed
	-- need to use os.epoch because its a game-wide variable
	if path:match("00%..+$") then -- regex should match 00.* at end of line, looking for first segment of song
		lastPlayed = now - 5000 -- lastPlayed to 5 sec ago so next loop plays immediately
	end
	
	-- make sure its been 5 seconds since last segment was played
	repeat
		os.sleep(0.05)
	until os.epoch("local") >= lastPlayed + 5000 -- each sound/music segment is exactly 5 seconds long. make sure 5sec have passed before attempting to play next segment
	print("Streaming..... epoch: "..os.epoch("local")) -- print a stimestamp for logging
	print("Δt                  : "..os.epoch("local")-lastPlayed)
	lastPlayed = os.epoch("local")
	if path:match("%.dfpwm$") then aukit.play(aukit.stream.dfpwm(data, 48000), peripheral.find "speaker")
	elseif path:match("%.wav$") then aukit.play(aukit.stream.wav(data), peripheral.find "speaker")
	elseif path:match("%.aiff?$") then aukit.play(aukit.stream.aiff(data), peripheral.find "speaker")
	elseif path:match("%.au$") then aukit.play(aukit.stream.au(data), peripheral.find "speaker")
	elseif path:match("%.flac$") then aukit.play(aukit.stream.flac(data), peripheral.find "speaker")
	else error("Unsupported file type.") end
	print("segment cached clock: "..os.epoch("local"))
	modem.transmit(43,15,{os.epoch("local"),1}) -- send reply with timestamp to see what the 
	end

-- s = os.clock() repeat print((s+5)-os.clock()) os.sleep(0.05) until os.clock() >= s + 5
