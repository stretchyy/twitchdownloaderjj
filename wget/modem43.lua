local modem = peripheral.find("modem") or error("No modem attached", 0)
modem.open(43) -- Open 43 so we can receive replies

-- Send our message
local sent = os.epoch()
modem.transmit(15, 43, "Hello, world!")
print("current epoch: "..sent)
print("message sent")

-- And wait for a reply
local loop = 0
while true do
	local event, side, channel, replyChannel, message, distance
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	until channel == 43
	loop = loop+1
	print("Received a reply: " .. tostring(message))
	dur = tonumber(message)
	ping = dur - sent
	ms = ping/72000
	print("time for computer "..loop.." to recieve: "..ms.." seconds")
end
