local modem = peripheral.find("modem") or error("No modem attached", 0)

local dfpwm = require("cc.audio.dfpwm")
local speakers = { peripheral.find("speaker") }

local songs ={}
songs[1] = ("test")
songs[2] = ("I dont wanna set the world on fire")

while true do
    term.clear()
    term.setCursorPos(1,1)
    
    print("select song")
    local input = tonumber(read())

    

    print ("now playing ")
    print (input..". "..songs[input])

    local song = ("data/"..songs[input]..".dfpwm")
    local decoder = dfpwm.make_decoder()
    
    
    for chunk in io.lines(song, 16 * 1024) do
    
        local buffer = decoder(chunk)
        for _, speaker in ipairs(speakers) do
        
 
            while not speaker.playAudio(buffer) do
                os.pullEvent("speaker_audio_empty")
                
            end

        end
    end
end