local modem = peripheral.find("modem") or error("No modem attached", 0)
modem.open(15)
while true do
	local event, side, channel, replyChannel, message, distance
	print("waiting for message...")
	repeat
		event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
		knob = os.epoch()
		rec = tostring(knob)
		-- print(("Message received on side %s on channel %d (reply to %d) from %f blocks away with message %s"):format(side, channel, replyChannel, distance, tostring(message)))
	until channel == 15
	modem.transmit(43,15,rec)
	print("message recieved, pretending to play music")
end
