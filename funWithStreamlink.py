import msvcrt
import sys
from collections import OrderedDict
from functools import partial
from itertools import chain

from streamlink.exceptions import *
from streamlink.stream import StreamIO
from streamlink.constants
from streamlink import Streamlink
from contextlib import closing
import os
import errno

from streamlink_cli.output import Output, FileOutput
from streamlink_cli.utils import Formatter

ACCEPTABLE_ERRNO = (errno.EPIPE, errno.EINVAL, errno.ECONNRESET)
STREAM_SYNONYMS = ["best", "worst", "best-unfiltered", "worst-unfiltered"]

streamlink: Streamlink = None
stream_fd: StreamIO = None
stdout = sys.stdout.buffer


"""class Output:
    def __init__(self):
        self.opened = False

    def open(self):
        self._open()
        self.opened = True

    def close(self):
        if self.opened:
            self._close()

        self.opened = False

    def write(self, data):
        if not self.opened:
            raise OSError("Output is not opened")

        return self._write(data)

    def _open(self):
        pass

    def _close(self):
        pass

    def _write(self, data):
        pass"""


"""class FileOutput(Output):
    def __init__(self, filename='banana.ts', fd=None, record=None):
        super().__init__()
        self.filename = filename
        self.fd = fd
        self.record = record

    def _open(self):
        if self.filename:
            self.fd = open(self.filename, "wb")

        if self.record:
            self.record.open()

        if os.name == "nt":
            msvcrt.setmode(self.fd.fileno(), os.O_BINARY)

    def _close(self):
        if self.fd is not sys.stdout.buffer:
            self.fd.close()
        if self.record:
            self.record.close()

    def _write(self, data):
        self.fd.write(data)
        if self.record:
            self.record.write(data)"""



# dooby = Output
output: Output = None
url = "https://www.twitch.tv/yogscast"
session = Streamlink()



def open_stream(stream):
    global stream_fd
    try:
        stream_fd = stream.open()
    except StreamError as err:
        raise StreamError(f"Could not open stream: {err}")

    try:
        prebuffer = stream_fd.read(8192)
    except OSError as err:
        stream_fd.close()
        raise StreamError(f"Failed to read data from stream: {err}")

    if not prebuffer:
        stream_fd.close()
        raise StreamError("No data returned from stream")

    return stream_fd, prebuffer


def read_stream(stream, output, prebuffer, formatter: Formatter, chunk_size=8192):
    stream_iterator = chain(
        [prebuffer],
        iter(partial(stream.read, chunk_size), b"")
    )
    try:
        for data in stream_iterator:
            try:
                output.write(data)
            except OSError as err:
                if err.errno in ACCEPTABLE_ERRNO:
                    print("Acceptable error ~leshrug~")
    except OSError as err:
        exit(f"Error when reading from stream: {err}, exiting")
    finally:
        stream.close()
        print("Stream ended")


def output_stream(stream, formatter: Formatter):
    global output

    success_open = False
    for i in range(30):
        try:
            stream_fd, prebuffer = open_stream(stream)
            success_open = True
            break
        except StreamError as err:
            exit()
    if not success_open:
        exit(f"Could not open stream {stream}, tried 30 times, exiting")

    output = FileOutput(fd=stdout)

    try:
        output.open()
    except OSError as err:
        exit(f"Failed to open output: {output.filename} ({err})")

    with closing(output):
        read_stream(stream_fd, output, prebuffer, formatter)
    return True


def resolve_stream_name(streams, stream_name):
    """Returns the real stream name of a synonym."""

    if stream_name in STREAM_SYNONYMS and stream_name in streams:
        for name, stream in streams.items():
            if stream is streams[stream_name] and name not in STREAM_SYNONYMS:
                return name

    return stream_name


def handle_stream(plugin, streams, stream_name):
    stream_name = resolve_stream_name(streams, stream_name)
    stream = streams[stream_name]
    # streams = session.streams(url)
    # stream = streams['best']
    alt_streams = list(filter(lambda k: stream_name + "_alt" in k,
                              sorted(streams.keys())))
    formatter = get_formatter(plugin)
    for stream_name in [stream_name] + alt_streams:
        stream = streams[stream_name]
        stream_type = type(stream).shortname()
        print(f"Opening stream: {stream_name} ({stream_type})")
        success = output_stream(stream, formatter)
    if success:
        return


url = "https://www.twitch.tv/yogscast"


def setup_plugin_options(streamlink, plugin):
    """Sets Streamlink plugin options."""
    session.set_option("ffmpeg-ffmpeg", 'C:/ProgramData/chocolateylib/ffmpeg/tools')
    session.set_option("ffmpeg-video-transcode", "h264")
    session.set_option("ffmpeg-audio-transcode", "aac")
    pass


def handle_url(url_):
    try:
        plugin = streamlink.resolve_url(url_)
        setup_plugin_options(streamlink, plugin)
        print(f"Found matching plugin {plugin.module} for URL {url_}")

    validstreams = format_valid_streams(plugin, streams)
    for stream_name in args.stream:
        if stream_name in streams:
            log.info(f"Available streams: {validstreams}")
            handle_stream(plugin, streams, stream_name)
            return


def setup_streamlink():
    """Creates the Streamlink session."""
    global streamlink

    streamlink = Streamlink()


if __name__ == '__main__':
    setup_streamlink()
    handle_url(url)