# Main file to run is monster.py

The rest are just for practice/experiments. 

Naming conventions for ALL files are either puns or entirely irrelevant to the project.  
Only naming conventions within the main monster.py file are (should be) sensible and make sense..... "*mostly™*".  
All other testing files are all over the place. They were never meant to be understood by anyone else, just enough for me to learn wtf I'm doing.



## Current (purpose)
Currently, the monster script runs two multiprocesses.
The first process:
* Downloads a live twitch stream (~700kb/s) using streamlink's CLI and outputs it to "JJ {month}-{day:02d} attempt {attempt}.ts". 
* The downloaded file is expected to be a 1080p60 .ts file.
* est. file size for 12 hours is ~20Gb  
* It will then hand the filename over to the second process via an intermediary "queue" of sorts.
    
The second process: 
* will wait in half-hour chunks of time
* every half hour it will check if a new item has been "added it to the queue" and then convert it.
    * the queue is a shared file that is read, added to a list, then deleted for sanity checks.
    * the queue file is accessed by a separate function to reduce risk of the file being open/read/written to simultaneously.  
* according to ffmpeg, convert speed is ~3.1x
* that wants to be converted into a 720p h264 acc -> mp4 video file.
* Additional ffmpeg arguments used: 
  - crf 18 (18 was recommended for some reason, can change if wanted)
  - preset veryfast (somehow has better compression than slow conversion)
  - movflags +faststart (moves various flags to the beginning of file. useful for streaming over LAN and faster loading times on slower devices)
    
The stream schedule has programs in 3 hour blocks and can also end abruptly for no reason.  
The maximum time I'm expecting the stream stream to go for each day is 12 hours.  
Therefore I have set the convert script to wait 1800sec/30min before checking if the download function has finished a download and has handed it off to the convert queue.  

The way the two processes interact with each other is via read/write of a file. Multiprocessing does have a manager that shares lists and functions and other goodies between processes, but as it turns out, uses much more resources and takes longer

For explanation see:  
<pre>
    https://stackoverflow.com/questions/13121790/using-multiprocessing-manager-list-instead-of-a-real-list-makes-the-calculation
</pre>


The process structure looks something to the extent of 

<pre>
TLDR: what it does
+-----------+--------------------------------------+  
|Process  1 |******#******#******#******#******#   |  
|         2 |~#~#~#~#***#~#***~#~#***#~#~#***#~#***|  Looks ugly, no?
+-----------+--------------------------------------+  
            + Time -->                             +  
            +--------------------------------------+  
process 1 is downloading process
process 2 is converting process
* is work, either waiting for work or actual work
# is beginning of new loop (be it download or checking queue)
~ is waiting for work
</pre>

Theres a bunch of details that are missing, I'm sure one or two might be important but I can't think of anything right now
    
##Known bugs
* The download and convert functions both use 'start time + 12 hours' as the time they're supposed to run for. Almost always, the download function will go longer for 12 hours, Dec-1  went for 15 hours. When the convert function reached 12 hours, it ended instead of waiting for the stream to end.
    * The signaling feature of multiprocessing might be able to help with this? It's unrealistic to just tell the convert function to continue to indefinitely wait until it finds something to convert ('butter.py' has multiprocessing disabled until solution implemented)
* Sometimes the download script will just... Stop... Or maybe its the streamlink cli that just quits, or hangs or gives up in an unexpected way? I've got no idea how to diagnose this one because it happens 3-12 hours into the stream while I'm asleep and I don't have a good logging system to know what happened. It happened once on Dec-1 just before the 13 hour mark of a 15 hour stream. It also happened after 3 hours during a 12 hour stream, it didn't even trigger the "add to queue" function that would have wrote the filename to the queue. It did create a 3 hour file, so even if the script had ended naturally, it would have checked if there was a file created.
    * The 13 hour ending was the result of the windows task scheduler being told to exit the task if it's still running after 13 hours. I've changed and fixed that. Still doesn't explain the 3 hour stop.
    * I suspect the download function exiting early is because the while loop only runs the now() function at the start of the first loop and it doesnt get run again? that shouldnt cause the problem, but the normal end of function print line is executed successfully and that that is the only check that can cause the loop to break, otherwise the loop should continue indefinitely. (potential fix applied to 'butter.py')
      * Try changing the while late > time_ loop from a datetime comparison to an integer comparison. just do "late - time_ > 0"
    * ...... *$@% I know what the problem is... Every time I did datetime.datetime.combine, i was using gmt.today(), when i should be using gmt.date(). gmt.today() converts gmt to local time, gmt.date() just returns the date from the datetime object... /facepalm     
##What "needs" to happen in the future.

So, I'll end up making a new file (butter.py) to work on as this one "works enough for now" but the way it works, is not right. The current one will only receive bug fixes when or if necessary.

1. Currently it uses the streamlink_cli, which is wrong (apparently). streamlink has its own API that I should be using... I just don't know how to use it "yet". Is very confusing to me  
I want to avoid using os.system(command) because from what I'm told, "it creates a new child-process" which is bad?... somehow? I didnt really understand the explanation. That said, looking through streamlink sourcecode, it also uses os.system for its ffmpeg piping just more *cleanly*, so I dont know if/why its such a big deal  
¯\\_( ツ )\_/¯    
2. Currently the two processes launch at the same time, one just waits 30min blocks for it to see if it needs to do something. If I'm going to continue to use multiprocessing, I should be using signaling
    <pre>
    What I want signaling to be able to do
    +-----------+--------------------------------------+
    |Process  1 |******#******#******#******#******    |
    |         2 |~~~~~~***~~~~***~~~~***~~~~***~~~~*** |
    +-----------+--------------------------------------+
                + Time -->                             +
               +--------------------------------------+
    process 1 is downloading process
    process 2 is converting process
    * is work, either waiting for work or actual work
    # is beginning of new loop (be it download or checking queue)
    ~ is waiting/halting, waiting for signal from process 1
    
    See:
    https://pymotw.com/2/multiprocessing/communication.html
    </pre>
    I will investigate the possibility of process 1 _launching_ process 2. If that can even be done or if p2 becomes a child of p1 and p2 needs to end before p1 can end. I need to learn more.
3. Currently, the script is just a launch with no arguments or options. I need to change this because it's current form is only useful during December of every year. There are lots of options and arguments I could be giving such and names, formatting, start times/durations, different stream channels, timezones, output dir, etc.
4. GUI. It's my intention to make this usable with a qt GUI. This alone requires me to learn how to use the streamlink API.
5. Currently, to start the script, you require to start it manually or add it to a task scheduler. I want a better to repeatedly start the script than with a task scheduler. I dont know if such a thing exists across platforms
6. Logging. Geez man, I definitely need better logging. Streamlink and FFmpeg both output to log.INFO with end='\r', so when theyre both running at the same time, the console is a real nightmare to read. would be nice if I could either separate the logs, or maybe just write them to log file and only keep the main functions logs in the console.  
Assuming I'm not there when the script starts, I'd like to be able to see the log output in case something went wrong. Currently, a shell pops up, and closes, no way to view the console log after script has finished.
7. jingle_my_jams needs optimising, or at least refining what it actually does. It used to need to do more, but all it needs to do now is return the time at which the script should stop trying to relaunch streamlink. Needs to handle knowing if stream is starting when the script starts or later.
8. jingle_my_jams, timeRemaining, startTime and finishTime all need to be aware of each other. I think... I need another function at the beginning of the script that determines whether to use datetime.now('gmt'), a predetermined starting/finish time, or a an expected duration or stream in (hrs, min)  




## Extras:

- So, streamlink can pipe the stream directly to ffmpeg, convert on the fly to a certain extent, and output to a file.  
- Converting on the fly, streamlink is unable to reduce the resolution of the video stream, plus it is unable to add the +faststart flag to the file.  
- All of that said, I want to keep the original .ts video file, so I don't think I can use on the fly converting anyway.  
- An inspection of one of the .ts files with ffprobe has revealed that the a/v codec is acc and h264. There also seems to be a third stream with an unsupported codec that I don't recognise or know where it comes from either. "Unsupported codec with id 100358 for input stream 2".   
- If converting doesnt change codec, I guess the main point of converting each video is to reduce filesize/resolution and add +faststart flag so it can be played on my phone.     

Side note:  
The crf 18 option combined with resolution reduction reduces the size of the file by up to 90% ZOMG!  
The faststart flag makes it so the file can seek better and more importantly can be played on my phone. Bonus, it makes it able to be played over the network with minimal hastle.  


# Butter.py
Butter.py is the continuation of monster.py. I left monster as is because I wanted its functionality for later use.  

## changes
* Removed all multiprocessing. End of download function now runs convert function by default, no option to disable
* streamlink should now convert to h264 and aac on the fly while downloading
* ffmpeg options now copy video and audio codec because h264 and aac should already be used.
  * ffmpeg should just resize to 720p, crf 18 and +faststart
* convert now deletes original .ts file after conversion to save space.
* Made it so download loop now only compares basic numbers instead of datetime objects
* fixed bug where datetime.combine was combining datetime.today instead of datetime.date

  