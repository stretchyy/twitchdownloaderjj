"""
    quality_override = ['None', 'best', '1080p60', '1080p', '720p60', '720p', '480p', '360p', '160p', 'worst', 'audio_only']
    https://www.riverbankcomputing.com/static/Docs/PyQt6/designer.html#using-the-generated-code
    https://doc-snapshots.qt.io/qt6-dev/qtdesigner-manual.html
    https://doc.qt.io/qt.html#qtforpython
"""
from PyQt6.QtWidgets import QDialog, QApplication
from ui_JingleJamGUI import Ui_HotStuff  # converted from JingleJamDownloader.ui
import streamlink
import sys


class ImageDialog(QDialog):
    def __init__(self):
        super().__init__()

        # Set up the user interface from Designer.
        self.ui = Ui_HotStuff()
        self.ui.setupUi(self)

        # Set up actions
        self.ui.URLbutton.clicked.connect(self.validate_url)
        self.ui.qualityoverride.currentIndexChanged.connect(self.overrideQuality)
        self.ui.streamquality.currentIndexChanged.connect(self.overrideQuality)

        # Add variables
        self.quality_override = ['None', 'best', '1080p60', '1080p', '720p60', '720p', '480p', '360p', '160p', 'worst',
                                 'audio_only']
        self.validated_qualities = []
        self.streamQuality = 'best'
        self.url = None
        self.output_directory = None
        self.download_filename = None
        self.convert_filename = None
        self.convert_resolution  = None
        self.setup()


    def validate_url(self):
        okay = self.ui.URLbox.text()
        try:
            strem = streamlink.streams(okay)
            if strem.__len__() > 1:
                self.url = okay
                self.ui.url_label.setText("----valid----")
                self.validated_qualities.clear()
                for j, k in strem:
                    self.validated_qualities.append(j)
            else:
                self.ui.url_label.setText("---invalid---")
                print("Invalid URL submitted")
        except:
            self.ui.url_label.setText("---invalid---")
            print("exception caught. Don't know what to do with it though ¯\_(ツ)_/¯")

    def setup(self):
        self.ui.qualityoverride.clear()
        for q in self.quality_override:
            self.ui.qualityoverride.addItem(q)

    def overrideQuality(self):
        if self.ui.qualityoverride.currentIndex() != 1:
            self.ui.streamquality.hide()
            self.streamQuality = self.quality_override[self.ui.qualityoverride.currentIndex()]
        else:
            self.ui.streamquality.show()
            self.streamQuality = self.validated_qualities[self.ui.streamquality.currentIndex()]


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = QDialog()
    window.show()
    sys.exit(app.exec())
