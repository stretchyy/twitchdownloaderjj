import datetime
import os
import pytz
import sys
from collections import deque
import multiprocessing
import time

"""
What still needs doing (next):
-at beginning of download function, add a "time now" (local and gmt) and "expected duration" in h:m:s print for sanity.
-do the multi processes need to be daemons still?


before "fully working:
add script to windows task scheduler
"""

# os.chdir(os.path.expanduser('~/Videos'))
os.chdir(
    "E:/JingleJam2021")  # its where i want the downloaded files to download for now. est filesize for 12 hours is ~20Gb
print("current directory: ", os.getcwd())


class Puppets:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = deque([])  # collections deque has better list functions for what im trying to do
        self.crash = False  #
        self.stream_url = None
        self.queue_filename = "convertqueue.txt"  # not sure i need this anymore but might be useful later
        self.test = False  # not needed anymore? was used to test functions and variables that are currently obsolete
        self.timezone = 'GMT'  # for setting up alternate timezones later
        self.working_directory = "E:/JingleJam2021"  # default working directory
        self.startTime = [11, 0]  # start time in 24-hour time (static for now. will make dynamic for gui)
        self.expectedDuration = [12, 0]  # expected duration of the FULL stream duration [hours, minutes]
        self.dir = kwargs.get('dir', "E:/JingleJam2021")  # for use with GUI
        self.original_stdout = None
        self.output = None

    def now(self, tz=None):
        if tz is None:
            return datetime.datetime.now()
        elif tz == self.timezone:
            bristol = pytz.timezone(self.timezone)
            gmt = datetime.datetime.now(bristol)
            gmtLessInfo = datetime.datetime.combine(gmt.today(), gmt.time())
            return gmtLessInfo

    def jingle_my_jams(self):
        bristol = pytz.timezone(self.timezone)
        gmt = datetime.datetime.now(bristol)  # Bristol time right now.
        # d = datetime.datetime.combine(gmt.today(), datetime.time(self.startTime[0], self.startTime[1]))
        # 11amGMT today
        d = datetime.datetime.combine(gmt.today(), gmt.time())  # assumes script starts at beginning of stream
        d_aware = bristol.localize(d)
        delta = d_aware - gmt
        m, s = divmod(delta.seconds, 60)
        h, m = divmod(m, 60)
        dd, h = divmod(h, 24)
        # print(f"Time until expected start- {h:02d}:{m:02d}:{s:02d}")
        late_ = d + datetime.timedelta(hours=self.expectedDuration[0], minutes=self.expectedDuration[1], seconds=10)
        print(f" Time now: {self.now(tz=self.timezone)}")
        # print(f"Start time at {d}")
        # print(f"finish time at {late_}")
        return d, late_

    def download(self, post_: bool, url_: str, koala: str = None):
        self.set_output()

        print("Download process has started")
        print("Download current directory: ", os.getcwd())

        attempt = 1
        cheese, late = self.jingle_my_jams()  # startTime, finishTime
        self.stream_url = url_
        if self.stream_url is None:
            return
        if koala is None:
            quality_ = 'best'
        else:
            quality_ = koala
        month = cheese.strftime('%b')  # String from time %b is month in Nov Dec etc
        day = cheese.day
        print(f"Downloading from stream {self.stream_url}")
        print(multiprocessing.current_process().name, "Entering while loop")
        while self.now(tz=self.timezone) < late:
            name = f"JJ {month}-{day:02d} attempt {attempt}"
            if os.path.isfile(f'{name}.ts'):
                print("For some reason the expected new filename already exists")
            twitch_command = f"powershell.exe streamlink.exe {self.stream_url} {quality_} -o '{name}.ts'"
            print(f"Name of next file: {name}")
            print(f"Console command: {twitch_command}")

            os.system(twitch_command)
            if os.path.isfile(f'{name}.ts'):
                """After something had downloaded, add it to the queue and modify the output name"""
                attempt += 1
                # self.queue.append(name)
                if post_:  # if initial run command says to, add to convert queue. enabled by default
                    print(f"Adding {name} to queue")
                    self.add_to_queue(name)
            else:
                """If nothing was added doesnt mean something went wrong..... But it probably did"""
                print("No new file was made")
                if self.now(tz=self.timezone) > late:
                    print("Chances are the stream has ended.")
                    print("----------Goodnight----------")
                    # break
                else:
                    print("Stream might be down or is changing POV, retrying......")
        print("EOF: Downloader process ending. its late")
        self.close_output()

    def convert(self):
        self.set_output()
        print("making sure convert process starts after download")
        time.sleep(5)
        print("convert process has started")
        print("convert current directory: ", os.getcwd())
        run_ = True
        notUsed, late_ = self.jingle_my_jams()
        while run_:
            self.import_queue()
            try:
                len_ = self.queue.__len__()
                for items in self.queue:
                    print(items)

                print(f"length of queue: {len_}")
                if len_ > 0:
                    title = self.queue.popleft()
                    print(multiprocessing.current_process().name, f"{len_} items in the queue.\nPROCESSING: {title}")
                    ffmpeg = \
                        f"-i '{title}.ts' -vf scale=-1:720 -crf 18 -preset veryfast -movflags +faststart '{title}.mp4'"
                    shell_command = f"powershell.exe ffmpeg.exe {ffmpeg}"
                    print("convert finished")
                    os.system(shell_command)
                else:
                    print("There are no tasks in the queue, waiting 30 min.")
                    time.sleep(1800)
            except AttributeError:
                print("Exception caught. There are no tasks in the queue, waiting 10min.")
                time.sleep(10)

            if self.now(tz=self.timezone) > late_ and self.queue.__len__() == 0:
                # after estimated time and if nothing in the queue
                run_ = False
        self.close_output()

    """
    Could just use multiprocessing but seems its a bad idea?
    https://stackoverflow.com/questions/13121790/using-multiprocessing-manager-list-instead-of-a-real-list-makes-the-calculation
    
    Tried using threading, but i wasnt sure it did what i wanted to do. couldnt test it within a reasonable timeframe
    see https://stackoverflow.com/a/55319297
    
    I could just use two scripts, but that's less fun and im not sure how to parse information between the two processes
    socket listen/send is NOT the answer.
    
    """

    def add_to_queue(self, name):
        try:
            if not os.path.isfile(f"{self.queue_filename}"):  # if queue file not there, make one.
                print("queue file not here")
                f = open(f"{self.queue_filename}", 'x')
                f.close()
            os.rename(self.queue_filename, self.queue_filename)  # test if file is open by renaming it
            with open(f"{self.queue_filename}", "a+") as file:  # add filename without extension to convert queue file
                print(
                    f"Queue text file location- {os.getcwd()}")  # to make sure export/import queue are in the same dir.
                leg = file.readline()  # just want to read the first line
                if leg == '':  # if first line empty, then queue file is empty
                    file.write(f"{name}" + "\n")  # then just add the item to the queue file
                    del leg
                else:  # well if the queue file still has finished what it's doing
                    file.write(f"{name}" + "\n")
                # if condition not necessary anymore, but too lazy to rewrite
                print(f"Added convert job '{name}' to queue")
        except PermissionError:
            print("I dont know why the file would be open already. but this is more important, will retry asap")
            # only reason this should be open, is because the import queue function has it open for a slipt second.
            time.sleep(1)
            self.add_to_queue(name)

    def import_queue(self):
        if os.path.isfile(f"{self.queue_filename}"):  # does file exist
            try:
                os.rename(self.queue_filename, self.queue_filename)  # test if file is open by renaming it
                with open(f"{self.queue_filename}", 'r') as file:
                    for x in file:
                        self.queue.append(x.rstrip())  # add items to end of queue by order of appearance
                print(self.queue)
                # assumed all items added, deleting the queue file is easier than overriding
                print("Queue file now empty, deleting")
                os.remove(f"{self.queue_filename}")
            except PermissionError:  # see print below
                print("Edge case exception, waiting 10 sec before retry. "
                      "Queue file was probably being accessed somewhere else.")
                time.sleep(10)  # i dont expect the file to be open by something else for more than half a second
                self.import_queue()  # i only expect to see this retried once.
        else:
            print("\n" + "Nothing new to add to the queue.")  # added \n to the beginning because streamlink adds \r
            # and would print this on the same line as the streamlink output

    def set_output(self):
        x = 1
        if x == 0:
            processName = multiprocessing.current_process().name
            self.original_stdout = sys.stdout
            self.output = open(f'{processName}_log.txt', 'w')
            sys.stdout = self.output

    def close_output(self):
        x = 1
        if x == 0:
            sys.stdout = self.original_stdout
            self.output.close()


"""when does multithread know when to terminate???"""


def run(url: str, quality: str = None, convert: bool = True):
    print("Init Puppet class")
    mainland = Puppets()
    # mainland.expectedDuration = [0, 0]
    if convert:
        print("Starting convert processes")
        one = multiprocessing.Process(target=mainland.download, daemon=True, name='Downloader',
                                      args=(convert, url, quality))
        two = multiprocessing.Process(target=mainland.convert, daemon=True, name='Converter:')
        one.start()
        two.start()
        one.join()
        two.join()
    else:
        print("Just downloading")
        mainland.download(convert, url, quality)
    print("Main function has ended.")


if __name__ == '__main__':
    run(convert=True, url="https://www.twitch.tv/yogscast", quality="best")

"""
FFMPEG INFO:

https://www.twitch.tv/videos/1214373145
moov atom/faststart
-movflags faststart
https://stackoverflow.com/questions/50914503/fastest-way-to-add-movflags-faststart-to-an-mp4-using-ffmpeg-leaving-everythi
https://superuser.com/questions/856025/any-downsides-to-always-using-the-movflags-faststart-parameter
http://ffmpeg.org/pipermail/ffmpeg-user/2021-January/051308.html
https://stackoverflow.com/questions/18294912/ffmpeg-generate-moov-atom/31147722
https://stackoverflow.com/questions/23787712/qt-faststart-windows-how-to-run-it

"""

"""
MULTIPROCESSING STUFF:

currently, the script launches two processes, download and convert, and runs them until theyre done.
    next iteration, i want to just have the download process run
    only when the download process has finished one loop, will it start the convert process.
    
    
something to the effect of  

what it currently does
+-----------+--------------------------------------+
|Process  1 |******#******#******#******#******#   |
|         2 |~#~#~#~#***#~#***~#~#***#~#~#***#~#***|
+-----------+--------------------------------------+
            + Time -->                             +
            +--------------------------------------+

what i want it to do
+-----------+--------------------------------------+
|Process  1 |******#******#******#******#******    |
|         2 |~~~~~~***~~~~***~~~~***~~~~***~~~~*** |
+-----------+--------------------------------------+
            + Time -->                             +
            +--------------------------------------+
where process 1 starts process 2, process 1 being able to terminate/end when its finished
* is work, either waiting for work or actual work
# is beginning of new loop
~ is waiting for work
process 1 is downloading process
process 2 is converting process
process 2 cannot start until process 1 has completed one loop of work
process 1 the mode work time before completion of one loop is 3 hours under normal circumstances
process 1 under normal circumstances, work time is in blocks of 3 hours, up to 12 hours (ie 3, 6, 9, 12hour blocks)
process 2 takes the work that process 1 has completed, and does more work on it.
process 2 work times are expected take not even half as long to complete as the process 1 took 

the point would be to save resources for when theyre not being used.
investigate https://stackoverflow.com/questions/31987207/put-multiple-items-in-a-python-queue
https://pymotw.com/2/multiprocessing/communication.html <---- this seems more appropriate with signaling
"""

"""
Next, use the streamlink API and stop using the streamlink cli as a child process
"""



