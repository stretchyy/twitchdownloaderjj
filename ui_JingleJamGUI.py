# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'JingleJamDownloaderfswVGh.ui'
##
## Created by: Qt User Interface Compiler version 6.1.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_HotStuff(object):
    def setupUi(self, HotStuff):
        if not HotStuff.objectName():
            HotStuff.setObjectName(u"HotStuff")
        HotStuff.resize(800, 600)
        self.centralwidget = QWidget(HotStuff)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayoutWidget = QWidget(self.centralwidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(30, 20, 741, 131))
        self.inputgridlayout = QGridLayout(self.gridLayoutWidget)
        self.inputgridlayout.setObjectName(u"inputgridlayout")
        self.inputgridlayout.setContentsMargins(0, 0, 0, 0)
        self.output_directory_button = QPushButton(self.gridLayoutWidget)
        self.output_directory_button.setObjectName(u"output_directory_button")

        self.inputgridlayout.addWidget(self.output_directory_button, 1, 2, 1, 1)

        self.directory_label = QLabel(self.gridLayoutWidget)
        self.directory_label.setObjectName(u"directory_label")

        self.inputgridlayout.addWidget(self.directory_label, 1, 0, 1, 1)

        self.filename_label = QLabel(self.gridLayoutWidget)
        self.filename_label.setObjectName(u"filename_label")

        self.inputgridlayout.addWidget(self.filename_label, 2, 0, 1, 1)

        self.filename_button = QPushButton(self.gridLayoutWidget)
        self.filename_button.setObjectName(u"filename_button")

        self.inputgridlayout.addWidget(self.filename_button, 2, 2, 1, 1)

        self.url_label = QLabel(self.gridLayoutWidget)
        self.url_label.setObjectName(u"url_label")

        self.inputgridlayout.addWidget(self.url_label, 0, 0, 1, 1)

        self.filename_box = QLineEdit(self.gridLayoutWidget)
        self.filename_box.setObjectName(u"filename_box")

        self.inputgridlayout.addWidget(self.filename_box, 2, 1, 1, 1)

        self.URLbutton = QPushButton(self.gridLayoutWidget)
        self.URLbutton.setObjectName(u"URLbutton")

        self.inputgridlayout.addWidget(self.URLbutton, 0, 2, 1, 1)

        self.output_directory_box = QLineEdit(self.gridLayoutWidget)
        self.output_directory_box.setObjectName(u"output_directory_box")
        self.output_directory_box.setReadOnly(True)

        self.inputgridlayout.addWidget(self.output_directory_box, 1, 1, 1, 1)

        self.URLbox = QLineEdit(self.gridLayoutWidget)
        self.URLbox.setObjectName(u"URLbox")

        self.inputgridlayout.addWidget(self.URLbox, 0, 1, 1, 1)

        self.commandlineoutput = QLineEdit(self.centralwidget)
        self.commandlineoutput.setObjectName(u"commandlineoutput")
        self.commandlineoutput.setGeometry(QRect(30, 380, 741, 22))
        self.commandlineoutput.setReadOnly(True)
        self.output_command_label = QLabel(self.centralwidget)
        self.output_command_label.setObjectName(u"output_command_label")
        self.output_command_label.setGeometry(QRect(40, 360, 121, 16))
        self.verticalLayoutWidget = QWidget(self.centralwidget)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(240, 160, 331, 91))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.qualitygrid = QGridLayout()
        self.qualitygrid.setObjectName(u"qualitygrid")
        self.streamquality = QComboBox(self.verticalLayoutWidget)
        self.streamquality.setObjectName(u"streamquality")

        self.qualitygrid.addWidget(self.streamquality, 1, 1, 1, 1)

        self.qualityoverride = QComboBox(self.verticalLayoutWidget)
        self.qualityoverride.setObjectName(u"qualityoverride")

        self.qualitygrid.addWidget(self.qualityoverride, 1, 2, 1, 1)

        self.Stream_Quality = QLabel(self.verticalLayoutWidget)
        self.Stream_Quality.setObjectName(u"Stream_Quality")
        sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Stream_Quality.sizePolicy().hasHeightForWidth())
        self.Stream_Quality.setSizePolicy(sizePolicy)
        self.Stream_Quality.setLayoutDirection(Qt.LeftToRight)
        self.Stream_Quality.setAutoFillBackground(False)
        self.Stream_Quality.setFrameShape(QFrame.NoFrame)

        self.qualitygrid.addWidget(self.Stream_Quality, 1, 0, 1, 1)

        self.label_7_override_stream_quality = QLabel(self.verticalLayoutWidget)
        self.label_7_override_stream_quality.setObjectName(u"label_7_override_stream_quality")
        self.label_7_override_stream_quality.setAlignment(Qt.AlignCenter)

        self.qualitygrid.addWidget(self.label_7_override_stream_quality, 0, 2, 1, 1)

        self.blankspacelabel = QLabel(self.verticalLayoutWidget)
        self.blankspacelabel.setObjectName(u"blankspacelabel")

        self.qualitygrid.addWidget(self.blankspacelabel, 0, 0, 1, 1)

        self.label_6_validated_stream_quality = QLabel(self.verticalLayoutWidget)
        self.label_6_validated_stream_quality.setObjectName(u"label_6_validated_stream_quality")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_6_validated_stream_quality.sizePolicy().hasHeightForWidth())
        self.label_6_validated_stream_quality.setSizePolicy(sizePolicy1)
        self.label_6_validated_stream_quality.setAlignment(Qt.AlignCenter)

        self.qualitygrid.addWidget(self.label_6_validated_stream_quality, 0, 1, 1, 1)


        self.verticalLayout.addLayout(self.qualitygrid)

        self.send_to_output = QPushButton(self.verticalLayoutWidget)
        self.send_to_output.setObjectName(u"send_to_output")
        self.send_to_output.setEnabled(True)

        self.verticalLayout.addWidget(self.send_to_output)

        self.verticalLayoutWidget_2 = QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(240, 260, 151, 31))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.convert_checkbox = QCheckBox(self.verticalLayoutWidget_2)
        self.convert_checkbox.setObjectName(u"convert_checkbox")

        self.verticalLayout_2.addWidget(self.convert_checkbox)

        self.handle_output = QPushButton(self.centralwidget)
        self.handle_output.setObjectName(u"handle_output")
        self.handle_output.setEnabled(True)
        self.handle_output.setGeometry(QRect(350, 450, 75, 24))
        self.outputlinetwo = QLineEdit(self.centralwidget)
        self.outputlinetwo.setObjectName(u"outputlinetwo")
        self.outputlinetwo.setGeometry(QRect(30, 410, 741, 22))
        self.outputlinetwo.setReadOnly(True)
        HotStuff.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(HotStuff)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 22))
        HotStuff.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(HotStuff)
        self.statusbar.setObjectName(u"statusbar")
        HotStuff.setStatusBar(self.statusbar)

        self.retranslateUi(HotStuff)
        self.send_to_output.clicked.connect(self.handle_output.show)

        QMetaObject.connectSlotsByName(HotStuff)
    # setupUi

    def retranslateUi(self, HotStuff):
        HotStuff.setWindowTitle(QCoreApplication.translate("HotStuff", u"Twitch Downloader CLI GUI", None))
        self.output_directory_button.setText(QCoreApplication.translate("HotStuff", u"Browse", None))
        self.directory_label.setText(QCoreApplication.translate("HotStuff", u"-------------", None))
        self.filename_label.setText(QCoreApplication.translate("HotStuff", u"-------------", None))
        self.filename_button.setText(QCoreApplication.translate("HotStuff", u"Check name", None))
        self.url_label.setText(QCoreApplication.translate("HotStuff", u"-------------", None))
        self.filename_box.setPlaceholderText(QCoreApplication.translate("HotStuff", u"filename", None))
        self.URLbutton.setText(QCoreApplication.translate("HotStuff", u"Validate URL", None))
        self.output_directory_box.setPlaceholderText(QCoreApplication.translate("HotStuff", u"output directory", None))
        self.URLbox.setPlaceholderText(QCoreApplication.translate("HotStuff", u"URL to stream or VOD", None))
        self.output_command_label.setText(QCoreApplication.translate("HotStuff", u"Output command :", None))
        self.Stream_Quality.setText(QCoreApplication.translate("HotStuff", u"Stream Quality", None))
        self.label_7_override_stream_quality.setText(QCoreApplication.translate("HotStuff", u"Override", None))
        self.blankspacelabel.setText("")
        self.label_6_validated_stream_quality.setText(QCoreApplication.translate("HotStuff", u"Validated", None))
        self.send_to_output.setText(QCoreApplication.translate("HotStuff", u"Send to output", None))
        self.convert_checkbox.setText(QCoreApplication.translate("HotStuff", u"convert after download", None))
        self.handle_output.setText(QCoreApplication.translate("HotStuff", u"Confirm!", None))
    # retranslateUi

