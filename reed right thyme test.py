import time
import os
from collections import deque

start = time.time()
queue = deque([])
queue_filename = "hamburger.txt"


def add_to_queue(name):
    try:
        if not os.path.isfile(f"{queue_filename}"):  # if queue file not there, make one.
            print("queue file not here")
            f = open(f"{queue_filename}", 'x')
            f.close()
        os.rename(queue_filename, queue_filename)  # test if file is open by renaming it
        with open(f"{queue_filename}", "a+") as file:  # add filename without extension to convert queue file
            print(f"Queue text file location- {os.getcwd()}")  # to make sure export and import queue are in the same dir.
            leg = file.readline()  # just want to read the first line
            if leg == '':  # if first line empty, then queue file is empty
                file.write(f"{name}" + "\n")  # then just add the item to the queue file
                del leg
            else:  # well if the queue file still has something in it then ffmpeg is probably doing something already
                file.write(f"{name}" + "\n")
            print(f"Added convert job '{name}' to queue")
    except PermissionError:  #
        print("I dont know why the file would be open already. but this is more important, will retry asap")
        time.sleep(1)
        add_to_queue(name)


def import_queue():
    if os.path.isfile(f"{queue_filename}"):  # does file exist
        try:
            os.rename(queue_filename, queue_filename)  # test if file is open by renaming it
            with open(f"{queue_filename}", 'r') as file:
                for x in file:
                    queue.append(x.rstrip())  # add items to end of queue by order of appearance
            print("Queue file now empty, deleting")
            print(queue)
            os.remove(f"{queue_filename}")  # assumed all items added, deleting the queue file is easier than overriding
        except PermissionError:  # see print below
            print("Edge case exception, waiting 10 sec before retry. "
                  "Queue file was probably being accessed somewhere else.")
            time.sleep(10)  # i dont expect the file to be open by something else for more than half a second
            import_queue()  # i only expect to see this retried once.
    else:
        print("Nothing new to add to the queue. Waiting for 10 seconds before retry")
        time.sleep(600)


for i in range(1, 6):
    add_to_queue(f"task no.{i}")  # add 5 tasks to queue file
import_queue()
for item in queue:
    print(item)

end = time.time()

duration = end - start
print(f"Time taken: {duration}")