import datetime
import os
import pytz
import sys
from collections import deque
import multiprocessing
import time

"""
To do:
convert all timezone checks to gmt only. don't swap between. too messy. 
    this should fix stream dropping over middnight issues
"""


# os.chdir(os.path.expanduser('~/Videos'))
os.chdir("E:/JingleJam2021")
print("current directory: ", os.getcwd())


class Puppets:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = deque([])  # collections deque has better list functions for what im trying to do
        self.crash = False  #
        self.stream_url = None
        self.queue_filename = "convertqueue.txt"  # not sure i need this anymore but might be useful later
        self.test = False  # not needed anymore? was used to test functions and variables that are currently obsolete
        self.timezone = 'GMT'  # for setting up alternate timezones later
        self.working_directory = "E:/JingleJam2021"  # default working directory
        self.startTime = [11, 0]  # start time in 24-hour time (static for now. will make dynamic for gui)
        self.expectedDuration = [12, 0]  # expected duration of the FULL stream duration [hours, minutes]
        self.finishTime = [23, 0]  # expected finish time in 24hr time [hour,min]
        self.finishDate = []
        self.dir = kwargs.get('dir', "E:/JingleJam2021")  # for use with GUI
        self.output = Wyrmwood()

    @staticmethod
    def now(tz=None):
        if tz is None:
            return datetime.datetime.now()  # returns local datetime object
        else:
            bristol = pytz.timezone(tz)
            gmt = datetime.datetime.now(bristol)
            gmtLessInfo = datetime.datetime.combine(gmt.date(), gmt.time())
            return gmtLessInfo  # returns datetime object

    def jingle_my_jams(self):
        """Returns when streamlink should stop retrying """
        print("Assuming script started at stream start time.")
        bristol = pytz.timezone(self.timezone)  # pruduces "<StaticTzInfo 'GMT'>" to be used with datetime tz
        gmt = datetime.datetime.now(bristol)  # gmt time right now with extra bits
        # d = datetime.datetime.combine(gmt.date(), datetime.time(self.startTime[0], self.startTime[1]))

        d = datetime.datetime.combine(gmt.date(), gmt.time())  # sanitize gmt time to simple datetime object

        # d_aware = bristol.localize(d)  # Are these useless???
        # delta = d_aware - gmt  # Are these useless???
        # m, s = divmod(delta.seconds, 60)  # Are these useless???
        # h, m = divmod(m, 60)  # Are these useless???
        # dd, h = divmod(h, 24)  # Are these useless???
        # print(f"Time until expected start- {h:02d}:{m:02d}:{s:02d}")  # Are these useless???

        late_ = d + datetime.timedelta(hours=self.expectedDuration[0], minutes=self.expectedDuration[1], seconds=10)
        print(f" Time now: {self.now(tz=self.timezone)}")
        print(f"Start time at {d}")
        print(f"finish time at {late_}")
        return d

    def timeRemaining(self):
        """time between now and finish time relative to timezone, default GMT. returns int."""
        bristol = pytz.timezone(self.timezone)
        gmtNow = datetime.datetime.now(bristol)
        nao = datetime.datetime.combine(gmtNow.date(), gmtNow.time())
        three, four = self.finishTime
        finnishTime = datetime.datetime.combine(gmtNow.date(), datetime.time(three, four))
        duration = finnishTime.timestamp() - nao.timestamp()  # time in seconds
        return duration

    def download(self, post_: bool, url_: str, koala: str = None):
        """
        Start here
        post_ : bool
            TRUE/FALSE, convert items after download
        url_ : str
            url of the stream
        koala : str
            quality of the stream, if not specified, 'best' will be used.
        """
        self.output.open(0)

        print("Download process has started")
        print("Download current directory: ", os.getcwd())

        attempt = 1
        cheese = self.jingle_my_jams()  # startTime, finishTime
        self.stream_url = url_
        if self.stream_url is None:
            return
        if koala is None:
            quality_ = 'best'
        else:
            quality_ = koala
        month = cheese.strftime('%b')  # String from time %b is month in Nov Dec etc
        day = cheese.day
        print(f"Downloading from stream {self.stream_url}")
        print(multiprocessing.current_process().name, "Entering while loop")
        time_ = self.timeRemaining()
        retry = 0
        while time_ > 0:
            print(f"{self.now(self.timezone)}")
            name = f"Yogurtcast {month}-{day:02d} attempt {attempt}"
            if os.path.isfile(f'{name}.ts'):
                print("For some reason the expected new filename already exists")
                # what cha gonna do about it?
            twitch_command = f"powershell.exe streamlink.exe {self.stream_url} {quality_} --ffmpeg-video-transcode " \
                             f"h264 --ffmpeg-audio-transcode aac --twitch-disable-hosting -o '{name}.ts' "
            print(f"Name of next file: {name}")
            print(f"Console command: {twitch_command}")

            os.system(twitch_command)
            if os.path.isfile(f'{name}.ts'):
                """After something had downloaded, add it to the queue and modify the output name"""
                attempt += 1
                retry = 0
                # self.queue.append(name)
                if post_:  # if initial run command says to, add to convert queue. enabled by default
                    print(f"Adding {name} to queue")
                    self.add_to_queue(name)
            else:
                # If nothing was added doesnt mean something went wrong..... But it probably did
                print("No new file was made")
                retry += 1
                if self.timeRemaining() < 0:
                    print("Chances are the stream has ended.")
                    print("----------Goodnight----------")
                    # break. theres no real need for this if statement other than to print something
                else:
                    print("Stream might be down or is changing POV, retrying......")
                    time.sleep(min(retry*2, 60))
                    # gradually sleep for longer so output log does get spammed with retry messages
            time_ = self.timeRemaining()
        print("EOF: Downloader function ended. its late")
        self.output.close(0)
        self.convert()

    def convert(self):
        self.output.open(0)
        print("making sure convert process starts after download")
        time.sleep(5)
        print("convert process has started")
        print("convert current directory: ", os.getcwd())
        run_ = True
        while run_:
            self.import_queue()
            try:
                len_ = self.queue.__len__()
                for items in self.queue:
                    print(items)

                print(f"length of queue: {len_}")
                if len_ > 0:
                    title = self.queue.popleft()
                    print(multiprocessing.current_process().name, f"{len_} items in the queue.\nPROCESSING: {title}")
                    ffmpeg = \
                        f"-i '{title}.ts' -vf scale=-1:720 -codec copy " \
                        f"-crf 18 -preset veryfast -movflags +faststart '{title}C.mp4'"
                    shell_command = f"powershell.exe ffmpeg.exe {ffmpeg}"
                    print("convert finished")
                    os.system(shell_command)
                    # os.remove(f"{title}.ts")
                else:
                    print("There are no tasks in the queue, waiting 30 min.")
                    time.sleep(1800)
            except AttributeError:
                print("Exception caught. There are no tasks in the queue, waiting 10min.")
                time.sleep(10)

            if self.queue.__len__() == 0:
                run_ = False
        self.output.close(0)

    """
    Could just use multiprocessing but seems its a bad idea?
    https://stackoverflow.com/questions/13121790/using-multiprocessing-manager-list-instead-of-a-real-list-makes-the-calculation

    Tried using threading, but i wasnt sure it did what i wanted to do. couldnt test it within a reasonable timeframe
    see https://stackoverflow.com/a/55319297

    I could just use two scripts, but that's less fun and im not sure how to parse information between the two processes
    socket listen/send is NOT the answer.

    """

    def add_to_queue(self, name):
        """Add downloaded file without file extension to convert queue"""
        try:
            if not os.path.isfile(f"{self.queue_filename}"):  # if queue file not there, make one.
                print("queue file not here")
                f = open(f"{self.queue_filename}", 'x')
                f.close()
            os.rename(self.queue_filename, self.queue_filename)  # test if file is open by renaming it
            with open(f"{self.queue_filename}", "a+") as file:  # add filename without extension to convert queue file
                print(
                    f"Queue text file location- {os.getcwd()}")  # to make sure export/import queue are in the same dir.
                leg = file.readline()  # just want to read the first line
                if leg == '':  # if first line empty, then queue file is empty
                    file.write(f"{name}" + "\n")  # then just add the item to the queue file
                    del leg
                else:  # well if the queue file still has finished what it's doing
                    file.write(f"{name}" + "\n")
                # if condition not necessary anymore, but too lazy to rewrite
                print(f"Added convert job '{name}' to queue")
        except PermissionError:
            print("I dont know why the file would be open already. but this is more important, will retry asap")
            # only reason this should be open, is because the import queue function has it open for a slipt second.
            time.sleep(1)
            self.add_to_queue(name)

    def import_queue(self):
        if os.path.isfile(f"{self.queue_filename}"):  # does file exist
            try:
                os.rename(self.queue_filename, self.queue_filename)  # test if file is open by renaming it
                with open(f"{self.queue_filename}", 'r') as file:
                    for x in file:
                        self.queue.append(x.rstrip())  # add items to end of queue by order of appearance
                print(self.queue)
                # assumed all items added, deleting the queue file is easier than overriding
                print("Queue file now empty, deleting")
                os.remove(f"{self.queue_filename}")
            except PermissionError:  # see print below
                print("Edge case exception, waiting 10 sec before retry. "
                      "Queue file was probably being accessed somewhere else.")
                time.sleep(10)  # i dont expect the file to be open by something else for more than half a second
                self.import_queue()  # i only expect to see this retried once.
        else:
            print("\n" + "Nothing new to add to the queue.")  # added \n to the beginning because streamlink adds \r
            # and would print this on the same line as the streamlink output


class Wyrmwood:
    def __init__(self):
        self.original_stdout = None
        self.output = None

    def open(self, code):
        x = code
        if x == 0:
            processName = 'help'
            self.original_stdout = sys.stdout
            self.output = open(f'{processName}_log.txt', 'a')
            sys.stdout = self.output

    def close(self, code):
        x = code
        if x == 0:
            sys.stdout = self.original_stdout
            self.output.close()


"""when does multithread know when to terminate??? daemon vs not"""


def run(url: str, quality: str = None, convert: bool = True):
    print("Init Puppet class")
    mainland = Puppets()
    # mainland.expectedDuration = [0, 0]
    mainland.download(convert, url, quality)
    mainland.convert()
    print("Main function has ended.")


if __name__ == '__main__':
    run(convert=True, url="https://www.twitch.tv/yogscast", quality="best")
