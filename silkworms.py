import queue
import time
import os
import random
import threading
from collections import deque


def test(commanding):
    now = time.time()
    os.system(commanding)
    print(time.time() - now)


def sample(booties):
    x = random.randrange(1, 5)
    time.sleep(x)
    print(f"I slept for {x} seconds, round {booties}")
    return f"COCKA {booties}"


class Slum:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = deque([])
        self.workers_rights_active = True
        self.super_ramp = 1
        self.logfile = None

    def worker(self):
        x = 0
        while x < 50:
            t = random.randrange(4, 15)
            print(t, "sleep 4")
            time.sleep(4)
            self.queue.append(t + random.random())
            print(threading.current_thread().getName(), f'added {t} to list')
            x += 1
        self.workers_rights_active = False
        return

    def ramp(self):
        end = 0
        while end == 0:
            len_ = self.queue.__len__()
            if len_ > 1:
                title = self.queue.popleft()
                print(threading.current_thread().getName(), f'thread waiting for {title} seconds')
                time.sleep(title)
                print("wait over")
            else:
                print("nothing to do, waiting.")
                time.sleep(5)
            if len_ == 0:
                if self.workers_rights_active is False:
                    end = 1

    def run(self):
        # https://stackoverflow.com/questions/15365406/run-class-methods-in-threads-python
        # one = threading.Thread(target=self.worker, daemon=True, name='worker')
        # two = threading.Thread(target=self.ramp, daemon=True, name='ramp')
        one = threading.Thread(target=self.worker, daemon=True, name='worker')
        # self.worker()  # fill queue
        two = threading.Thread(target=self.ramp, daemon=True, name='ramp')
        one.start()
        two.start()
        one.join()
        two.join()


socka = Slum()
socka.run()
exit(1)

# send thirty task requests to the worker
for item in range(5):
    f = (item + 1) * 100
    command = f"powershell.exe ffmpeg -y -f lavfi -i nullsrc=s=hd720:d=60 " \
              f"-movflags +faststart -preset ultrafast out{item}.mp4"

# ffmpeg -y -f lavfi -i nullsrc=s=hd720:d=600 -movflags +faststart -preset ultrafast out.mp4
