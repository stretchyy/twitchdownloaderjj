import datetime
import os
import pytz
import sys
from collections import deque
import threading
import time

# os.chdir(os.path.expanduser('~/Videos'))
os.chdir("E:/JingleJam2021")
print("current directory: ", os.getcwd())


class Puppets:
    def __init__(self, campfire, url=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = deque([])
        self.active = True
        self.crash = False
        self.logfile = campfire
        self.stream_url = url

    @staticmethod
    def now():
        x = datetime.datetime.now()
        return x

    @staticmethod
    def jingle_my_jams():
        bristol = pytz.timezone('GMT')
        gmt = datetime.datetime.now(bristol)
        d = datetime.datetime.combine(gmt.today(), datetime.time(11, 0))  # 11am GMT datetime-object (year, M, D, H, m)
        if d.month is not 12:
            print("It's not even December. I shouldn't even be running, but here we are.")
            return "early", "too soon"
        d_aware = bristol.localize(d)
        delta = d_aware - gmt
        m, s = divmod(delta.seconds, 60)
        h, m = divmod(m, 60)
        dd, h = divmod(h, 24)
        timeframe = [12 - d.month, h, m, s]
        print(f"{h:02d}:{m:02d}:{s:02d}")
        late_ = d + datetime.timedelta(hours=12)
        # print(str(delta))
        return timeframe[1], late_
        # timeframe is time until \"steam start\". late_ is the time when stream \"should end\"

    def download(self):
        tea = 1
        day, late = self.jingle_my_jams()
        # if frame == "early":
        #     return None
        url = "https://www.twitch.tv/yogscast"
        if self.stream_url is None:
            return
        print(f"Downloading from stream {url}")
        print("Entering while loop")
        while self.now() < late:
            name = f"JJ Dec-{day} attempt {tea}"
            twitch_command = f"powershell.exe streamlink.exe {url} best -o \'{name}.ts\'"
            os.system(twitch_command)
            if os.path.isfile(name):
                """After something had downloaded, add it to the queue and modify the output name"""
                tea += 1
                self.queue.append(name)
            else:
                """If nothing was added doesnt mean something went wrong..... But it probably did"""
                print("No new file was made")
                if self.now() > late:
                    print("Chances are the stream has ended.")
                    print("----------Goodnight----------")
                    break
                else:
                    print("Stream might be down or is changing POV, retrying......")

    def convert(self):
        end = False
        while end is False:
            len_ = self.queue.__len__()
            if len_ > 0:
                title = self.queue.popleft()
                print(threading.current_thread().getName(), f"{len_} items in the queue.\nPROCESSING: {title}")
                ffmpeg = f"-i {title}.ts -vf scale=-1:720 -crf 18 -preset veryfast -movflags +faststart {title}.m4a"
                powershell_command = f"powershell.exe ffmpeg.exe {ffmpeg}"
                print()
                os.system(powershell_command)
            else:
                print("There are no tasks in the queue, waiting 10min.")
                time.sleep(600)

    def run(self):
        sys.stdout = self.logfile
        one = threading.Thread(target=self.download, daemon=True, name='Downloader:')
        two = threading.Thread(target=self.convert, daemon=True, name='Converter:')
        one.start()
        two.start()
        one.join()
        two.join()


if __name__ == "__name__":
    logfile = open('streamlink_log.txt', 'w')
    original_stdout = sys.stdout
    # sys.stdout = logfile
    main = Puppets(logfile)
    main.run()
    sys.stdout = original_stdout
    logfile.close()



"""
moov atom/faststart
-movflags faststart
https://stackoverflow.com/questions/50914503/fastest-way-to-add-movflags-faststart-to-an-mp4-using-ffmpeg-leaving-everythi
https://superuser.com/questions/856025/any-downsides-to-always-using-the-movflags-faststart-parameter
http://ffmpeg.org/pipermail/ffmpeg-user/2021-January/051308.html
https://stackoverflow.com/questions/18294912/ffmpeg-generate-moov-atom/31147722
https://stackoverflow.com/questions/23787712/qt-faststart-windows-how-to-run-it

"""