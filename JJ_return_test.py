"""
the purpose of the function needs to return the following:
[Month in '%b', day of month in int()], end time that can be compared with datetime.datetime.now
assumes script has started at the right time.
"""

import datetime
import pytz


class Poppy:
    def __init__(self):
        self.timezone = 'GMT'
        self.startTime = [11, 0]
        self.expectedDuration = [12, 0]

    def now(self, tz=None):
        if tz is None:
            return datetime.datetime.now()
        elif tz == self.timezone:
            bristol = pytz.timezone(self.timezone)
            gmt = datetime.datetime.now(bristol)
            gmtLessInfo = datetime.datetime.combine(gmt.today(), gmt.time())
            return gmtLessInfo

    def jingle_my_jams(self):
        bristol = pytz.timezone(self.timezone)
        gmt = datetime.datetime.now(bristol)  # Bristol time right now.
        # d = datetime.datetime.combine(gmt.today(), datetime.time(self.startTime[0], self.startTime[1]))  # 11am GMT
        d = datetime.datetime.combine(gmt.today(), gmt.time())
        d_aware = bristol.localize(d)
        delta = d_aware - gmt
        m, s = divmod(delta.seconds, 60)
        h, m = divmod(m, 60)
        dd, h = divmod(h, 24)
        print(f"{h:02d}:{m:02d}:{s:02d}")
        late_ = d + datetime.timedelta(hours=self.expectedDuration[0], minutes=self.expectedDuration[1])
        print(f"time now: {self.now(tz=self.timezone)}")
        print(f"Start time at {d}")
        print(f"finish time: {late_}")
        print(f"time until start: {delta}")
        print(delta.seconds)  # time until stream in seconds.


lamb = Poppy()
lamb.jingle_my_jams()
